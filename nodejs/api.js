var user = require('./lib/user');
var format = require('./lib/format');


router.get('/api/user',
function(req, res, next) {                              // if user authenticated
    if (!req.isAuthenticated || !req.isAuthenticated()) {
        res.send(401);
        return;
    }
    next();
},
function(req, res, next) {
  // if(req.user.strategy == 'google')
  // {
    console.log(req.user.strategy + ' strategy');
    var user_data, all_formats;
    user.getUser('User', req.user.id, function(user_data){
      //console.log(JSON.stringify(data.formatIds, null, 2));
      var formatsToSend = [];
      // var formats= [{id: 'chapterize', name: 'Chatpterize'}, {id: 'tubequiz', name: 'TubeQuiz'}, {id: 'mixtape', name: 'Mixtape'}, {id: 'dummy', name: 'Dummy'}, {id: 'annotator', name: 'Annotator'}];
      // for(var i in data.formatIds.L){
      //   //console.log(data.formatIds.L[i].S);
      //   for(var j in formats){
      //     if(formats[j].id == data.formatIds.L[i].S)formatsToSend.push({id: formats[j].id, name: formats[j].name})
      //   }
      // }

      format.getAll(function(all_formats){
        for(var i in all_formats.Items){
          for(var j in user_data.formatIds.L){
            if(all_formats.Items[i].id ==user_data.formatIds.L[j].S)formatsToSend.push({id: all_formats.Items[i].id, name: all_formats.Items[i].name,description: all_formats.Items[i].description})
          }
        }
        var userObject = {id : req.user.id, name : req.user.name, nickname:user_data.nickname.S, formats : formatsToSend}

        // console.log(JSON.stringify(userObject, null, 2));
        res.send(JSON.stringify(userObject));
      })



    })
});
