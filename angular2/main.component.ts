import {Component} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import {Router} from '@angular/router-deprecated';

import {Session} from './session';

@Component({
    selector: 'main-page',
    template: `
    <div class = "container">
      <h1>Welcome to supemashapp</h1>

      <div *ngIf="session.user">
        <div style="margin-bottom: 3em;">
          <h3><b>{{session.user.name}}</b></h3>
          <p>Create your own video mashapp story and share it with the world <br> by using SuperMashApp embeddable HTML5 player</p>
          <button type="button" class="btn btn-info" (click)="goToCreate();">Create mashapp</button>
        </div>
        <div>
          Your SuperMashApp username is
          <h4><b>{{session.user.nickname}}</b></h4>

          <div *ngIf="nicknameCheck()">
            <p>To change and register a permanent username click here:</p>
            <button type="button" class="btn btn-info" (click)="goToProfile();">Edit username</button>
          </div>
        </div>
      </div>
      <div *ngIf="!session.user">
        <p>Please login</p>
        <button type="button" class="btn btn-primary" (click)="openLogin()">Login</button>
        <br><br>
        <p>or try the editor as a guest</p>
        <button type="button" class="btn btn-primary" (click)="test()">Try it now!</button>
      <div>

    </div>
    `,
    styles: [`
    `]
})



export class MainPageComponent {

  sessionStarted:boolean = false;
  _router:Router;
  constructor(private session:Session, private http:Http,public router: Router){
    this._router = router;
  }

  nicknameCheck(){
    if((this.session.user.nickname.indexOf('AnonymousDJ') > -1))return true;
    else return false;
  }
  test(){
    alert("Working!");
  }

  updateLoginModal() {
    if (this.session.user) {
      window['jQuery']('#session-modal').modal('hide');
    } else {
      window['jQuery']('#session-modal').modal('show');
    }
  }
  getUser() {
    this.http.get('/api/user/').map((res) => {
      return res.json();
    }).subscribe(
      (data) => {
        this.sessionStarted = true;
        this.session.user = data;
        this.updateLoginModal();
      },
      (err) => {
        this.session.user = null;
        this.updateLoginModal();
      }
    );
  }

  openLogin() {

    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen['left'];
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen['top'];
    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
    var w = Math.max(500, width * 0.6);
    var h = Math.max(500, height * 0.6);
//    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
//    var top = ((height / 2) - (h / 2)) + dualScreenTop;
    var left = (width * 0.1) + dualScreenLeft;
    var top = (height * 0.1) + dualScreenTop;

    var popup = window.open('/login/','smapp-popup', 'width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

    var url = undefined;
    var timer = setInterval(() => {
      try {
        if (popup.location.href != url) {
          url = popup.location.href;
          if (url.search("login-successful") != -1) {
            popup.close()
            this.getUser();
            this.router.navigate(['MainPage']);
          }
        }
      } catch (e) {
      }
      if (popup.closed !== false) { // !== is required for compatibility with Opera
          clearInterval(timer);
      }
    }, 200);
  }

  goToCreate(){
    this.router.navigate(['CreateContent']);
  }

  goToProfile(){
    this.router.navigate(['EditProfile']);
  }
}