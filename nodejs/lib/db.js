var AWS = require('aws-sdk');

var checkIfExists = function(params,id ,callback){

  AWS.config.update({
    region: "us-east-1",
  });

  var dynamodb = new AWS.DynamoDB();

  dynamodb.getItem(params, function(err, data) {
   if (err) {
     console.log(err);
     //callback();
     }
   else {
     //callback(data.Item);
    //  console.log(data);

    console.log('Checking user');
    if(data.Item === undefined){
      callback(id);
      return false;
    }
    else return true;
    }
  });
}

var getElement = function(params,callback){
  AWS.config.update({
    region: "us-east-1",
  });

  var dynamodb = new AWS.DynamoDB();

  dynamodb.getItem(params, function(err, data) {
   if (err) {
     console.log(err);
     }
   else {
     callback(data.Item);
    // console.log(JSON.stringify(data, null, 2));
    }
  });
}

var query = function(params,callback){
  AWS.config.update({
    region: "us-east-1",
  });

  var dynamodbDoc = new AWS.DynamoDB.DocumentClient();

  dynamodbDoc.query(params, function(err, data) {
      if (err) {
          console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
      } else {
          console.log("Query succeeded.");
          //console.log('Data comes here',data);
          callback(data)
          data.Items.forEach(function(item) {
              //console.log(item);
          });
      }
  });

}


var getElementBySecIndex = function(params,callback){          // name should be changed(using query)

  AWS.config.update({
    region: "us-east-1",
  });

  var dynamodb = new AWS.DynamoDB();
  dynamodb.query(params, function(err, data) {
      if (err) {
          console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
      } else {
          console.log("Query succeeded.");
          // data.Items.forEach(function(item) {
          //   //  console.log(item);
          // });
          callback(data);
      }
  });

}

var saveElement = function(params, callback){

  AWS.config.update({
    region: "us-east-1",
  });

  var dynamodbDoc = new AWS.DynamoDB.DocumentClient();

  console.log("Adding a new item...");
  dynamodbDoc.put(params, function(err, data) {
      if (err) {
          console.error("Unable to add item. Error JSON:", JSON.stringify(err, null, 2));
      } else {
          console.log("Added item:", JSON.stringify(data, null, 2));
          callback();
      }
  });
}

var deleteElement = function(params, callback){

  AWS.config.update({
    region: "us-east-1",
  });

  var dynamodbDoc = new AWS.DynamoDB.DocumentClient();

  console.log("Attempting to delete data...");
  dynamodbDoc.delete(params, function(err, data) {
      if (err) {
          console.error("Unable to delete item. Error JSON:", JSON.stringify(err, null, 2));
      } else {
          console.log("DeleteItem succeeded:", JSON.stringify(data, null, 2));
          callback();
      }
  });
}

var updateElement = function(params, callback){

  AWS.config.update({
    region: "us-east-1",
  });
  
  var dynamodbDoc = new AWS.DynamoDB.DocumentClient();

  console.log("Updating the item...");
  dynamodbDoc.update(params, function(err, data) {
      if (err) {
          console.error("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
      } else {
          console.log("UpdateItem succeeded:", JSON.stringify(data, null, 2));
          callback();
      }
  });
}

var scan = function(params, callback){

  AWS.config.update({
    region: "us-east-1",
  });

  var docClient = new AWS.DynamoDB.DocumentClient();
  docClient.scan(params, onScan);

  function onScan(err, data) {
      if (err) {
          console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));
      } else {
          // print all the movies
          console.log("Scan succeeded.");
          // data.Items.forEach(function() {
          //    console.log(data);
          // });
          callback(data);

          // continue scanning if we have more movies
          if (typeof data.LastEvaluatedKey != "undefined") {
              console.log("Scanning for more...");
              params.ExclusiveStartKey = data.LastEvaluatedKey;
              docClient.scan(params, onScan);
          }
      }
  }
}

module.exports.checkIfExists = checkIfExists;
module.exports.getElement = getElement;
module.exports.query = query;
module.exports.getElementBySecIndex = getElementBySecIndex;
module.exports.saveElement = saveElement;
module.exports.deleteElement = deleteElement;
module.exports.updateElement = updateElement;
module.exports.scan = scan;
