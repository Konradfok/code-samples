var db = require('./db');
var crypto = require('crypto');

var dateGenerate = function(){
  var d = new Date();
  var n = d.getTime();

  return n;
}

var userAdminCheck = function(id, callback){


  console.log(id);
  var params = {                      //params for getitem(aws-sdk) are slightly different
    TableName : 'User',
    Key : {
     "id" : {
      "S" : id
    },
   }
  }
  db.checkIfExists(params, callback);    //uses getItem from aws-sdk
}


var deleteUser = function(id, res){

  var params = {
    TableName:'User',
    Key:{
        "id":id,
    },

};

  db.deleteElement(params, res);

}
var updateUser = function(id,nickname, callback){
  var params = {
      TableName:'User',
      Key:{
          "id": id,
      },
      UpdateExpression: "set nickname = :r",
      ExpressionAttributeValues:{
          ":r": nickname,
      },
      ReturnValues:"UPDATED_NEW"
  };

  db.updateElement(params, callback);
}


// editor's part

var userLoginDataCheck = function(provider,id, callback){

console.log('Checking UserLoginData');
  var params = {
      TableName : "UserLoginData",
      KeyConditionExpression: "#na = :yyyy and #ha = :zzzz",
      ExpressionAttributeNames:{
          "#na": "provider",
          "#ha": "id"
      },
      ExpressionAttributeValues: {
          ":yyyy": provider,
          ":zzzz": id

      }

  };
  db.query(params, callback);    //uses getItem from aws-sdk
}

var userCheck = function(id, callback){
  console.log('Checking User');
  var params = {                      //params for getitem(aws-sdk) are slightly different
    TableName : 'User',
    Key : {
     "id" : {
      "S" : id
    },
   }
  }
  if(db.checkIfExists(params, id ,callback))
  {
    var id = crypto.randomBytes(20).toString('hex');
    userCheck(id,callback);
  }
}

var createUserLoginData = function(provider, id, user_id, callback){
  console.log('Creating UserLoginData');
  var table = "UserLoginData";
  var params = {
      TableName:table,
      Item:{
          "provider": provider,
          "id" : id,
          "user_id" : user_id,
      }
  };
  db.saveElement(params, callback);
}

var getUser = function(table, id, callback){
  var params = {                      //params for getitem(aws-sdk) are slightly different
    TableName : table,
    Key : {
     "id" : {
      "S" : id
    },
   }
  }
  db.getElement(params,callback);
}


//common part for Admin and editor

var createUser = function(id,name,formatIds, callback){
  console.log('Creating User');
  var table = "User";
  var created = dateGenerate();
  var params = {
      TableName:table,
      Item:{
          "id": id,
          "created" : created,
          "name" : name,
          "formatIds" : formatIds,
          "nickname" : 'AnonymousDJ' + String(created)
      }
  };
  db.saveElement(params, callback);
}

var checkNickname = function(nickname,callback){
  var params = {
          TableName : 'User',
          IndexName : 'nickname-index',
          KeyConditions :
          {
              "nickname" :
              {
                  "AttributeValueList" : [
                  {
                      'S' : nickname,
                  }
                  ],
                  "ComparisonOperator" : "EQ"
              }
          },
      }
  db.getElementBySecIndex(params, callback);
}

module.exports.userAdminCheck = userAdminCheck;
module.exports.createUser = createUser;
module.exports.deleteUser = deleteUser;
module.exports.updateUser = updateUser;

module.exports.userLoginDataCheck = userLoginDataCheck;
module.exports.userCheck = userCheck;
module.exports.createUser = createUser;
module.exports.createUserLoginData = createUserLoginData;
module.exports.getUser = getUser;
module.exports.checkNickname = checkNickname;
