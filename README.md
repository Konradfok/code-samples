nodejs folder contains api sample code used in my recent project. db.js file is a kind of library I created for accessing AWS dynamodb. I used Amazon examples. 

Angular2 was of course used for front-end. main.component.ts is basically a login page. duration component.ts is a small component used for calculating duration based on start and stop inputs.

This code will not work without a proper setup of angular2,  nodejs and dynamodb.