import {Input, Output, Injectable, EventEmitter, Component} from '@angular/core';

@Injectable()
@Component({
    selector: 'duration',
    template: `
    <div>
      <label>Duration time:  </label>
      <span *ngIf="currentHours!=0">{{currentHours}}:</span>
      <span>{{currentMinutes}}:{{currentSeconds}}</span>
    </div>
    `,
    styles: [`
    `]
})

export class DurationComponent {

  @Input() start;
  @Input() stop;
  video_duration:number;

  currentHours:number = 0;
  currentMinutes:number = 0;
  currentSeconds:number = 0;

  constructor() {
  }

  ngOnChanges(){
    this.video_duration = this.stop - this.start;

    this.currentHours = Math.floor(this.video_duration / 3600);
    var totalSec = this.video_duration % 3600;
    this.currentMinutes = Math.floor(totalSec / 60);
    this.currentSeconds = totalSec % 60;
  }

}
