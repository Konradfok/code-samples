var db = require('./db');


var deleteFormat = function(id, res){
  var params = {
    TableName:'Format',
    Key:{
        "id":id,
    },

};

  db.deleteElement(params, res);
}

var addFormat = function(id,name,description,public,defaults, baseMagicastAsset, callback){
  var params = {
      TableName:'Format',
      Item:{
          "id": id,
          "name" : name,
          "description" : description,
          "public" : public,
          "defaults" : JSON.stringify(defaults),
          "baseMagicastAsset" : baseMagicastAsset
      }
  };
  db.saveElement(params, callback);
}

var getFormat = function(id,callback){
  var params = {                      //params for getitem(aws-sdk) are slightly different
    TableName : 'Format',
    Key : {
     "id" : {
      "S" : id
    },
   }
  }
  db.getElement(params,callback);
}

var getAll = function(callback){
  var params = {
      TableName: "Format",
  };
  db.scan(params,callback);
}



module.exports.getFormat = getFormat;
module.exports.getAll = getAll;
